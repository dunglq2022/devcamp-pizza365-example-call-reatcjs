import { Component, React } from "react";

class FecthComponent extends Component {
    fetchApi = async (url, requestOptions) => {
        let response = await fetch(url, requestOptions);
        let data = await response.json();

        return (data)
    }
    

    getAllOrderHandle = () => {
        var requestOptions = {
            method: 'GET',
            redirect: 'follow'
          };

          var url = "http://203.171.20.210:8080/devcamp-pizza365/orders";

          this.fetchApi(url, requestOptions)
            .then((response) => {
                console.log(response);
            })
            .catch((error) => {
                console.error(error);
            })
    }

    getOderById = () => {
        var requestOptions = {
            method: 'GET',
            redirect: 'follow'
          };

        var url = "http://203.171.20.210:8080/devcamp-pizza365/orders/PJ1KtZLqMZ"

        this.fetchApi(url, requestOptions)
            .then((response) => {
                console.log(response);
            })
            .catch((error) => {
                console.error(error);
            })
    }

    createOrder = () => {
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");

        var raw = JSON.stringify({
            "kichCo": "L",
            "duongKinh": "30",
            "suon": 8,
            "salad": "500",
            "loaiPizza": "Hawaii",
            "idVourcher": null,
            "thanhTien": 250000,
            "giamGia": 0,
            "idLoaiNuocUong": "TRASUA",
            "soLuongNuoc": 4,
            "hoTen": "Lê Quốc Dũng",
            "email": "tddv2017@gmail.com",
            "soDienThoai": "0931497764",
            "diaChi": "Hồ Chí Minh",
            "loiNhan": "ok",
            "trangThai": "cancel",
            "ngayTao": 1680701190160,
            "ngayCapNhat": 1680710516981
        });

        var requestOptions = {
        method: 'POST',
        headers: myHeaders,
        body: raw,
        redirect: 'follow'
        };

        var url = "http://203.171.20.210:8080/devcamp-pizza365/orders/"

        this.fetchApi(url, requestOptions)
            .then((response) => {
                console.log(response);
            })
            .catch((error) => {
                console.error(error);
            })
    }

    updateOrderById = () => {
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");

        var raw = JSON.stringify({
            "kichCo": "L",
            "duongKinh": "30",
            "suon": 8,
            "salad": "Test update",
            "loaiPizza": "Test update",
            "idVourcher": null,
            "thanhTien": 250000,
            "giamGia": 0,
            "idLoaiNuocUong": "Test update",
            "soLuongNuoc": 4,
            "hoTen": "Lê Quốc Dũng Test update",
            "email": "tddv2017@gmail.com",
            "soDienThoai": "0931497764",
            "diaChi": "Hồ Chí Minh",
            "loiNhan": "ok",
            "trangThai": "cancel",
            "ngayTao": 1680701190160,
            "ngayCapNhat": 1680710516981
        });

        var requestOptions = {
        method: 'PUT',
        headers: myHeaders,
        body: raw,
        redirect: 'follow'
        };

        var url = "http://203.171.20.210:8080/devcamp-pizza365/orders/159276"

        this.fetchApi(url, requestOptions)
            .then((response) => {
                console.log(response);
            })
            .catch((error) => {
                console.error(error);
            })
    }

    checkVoucherById = () => {
        var requestOptions = {
            method: 'GET',
            redirect: 'follow'
          };

        var url = "http://203.171.20.210:8080/devcamp-pizza365/voucher_detail/12355"

        this.fetchApi(url, requestOptions)
        .then((response) => {
            console.log("Voucher:");
            console.log(response);
        })
        .catch((error) => {
            console.log("Không tìm thấy voucher" );
            console.log(error);
        })
    }

    getDrinkList = () => {
        var requestOptions = {
            method: 'GET',
            redirect: 'follow'
          };

          var url = "http://203.171.20.210:8080/devcamp-pizza365/drinks"

          this.fetchApi(url, requestOptions)
          .then((response) => {
            console.log("Drinks:");
            console.log(response);
          })
    }

    render() {
        return (
            <>
            <h3 className="text-center">Fetch Api</h3>
            <div className="bg-light">
                <div className="container">
                    <div className="form-group">
                        <p id="cmt2dev">Test Page for Javascrip Tasks. F5 to run code. </p>
                    </div>
                    <div className="form-group">
                    <form id="singleForm w-75">
                        <input type="button" className="btn btn-primary me-2" onClick= {this.getAllOrderHandle} value="Call api get all orders!"/>
                        <input type="button" className="btn btn-info me-2" onClick={this.createOrder} value="Call api create order!"/>
                        <input type="button" className="btn btn-success me-2" onClick={this.getOderById} value="Call api get order by id!"/>
                        <input type="button"  className="btn btn-secondary me-2" onClick={this.updateOrderById} value="Call api update order!"/>
                        <input type="button"  className="btn btn-danger me-2" onClick={this.checkVoucherById} value="Call api check voucher by id!"/>
                        <input type="button"  className="btn btn-success me-2" onClick={this.getDrinkList}value="Call api Get drink list!"/> 
                    </form>
                </div>
                <div className="form-group">
                    <p id="testP" style={{fontSize:'larger'}}> Demo 06 API for Pizza 365 Project: </p>
                    <ul>
                        <li>get all Orders: lấy tất cả orders </li>
                        <li>create Order: tạo 1 order</li>
                        <li>get Order by ID: lấy 1 order bằng ID </li>
                        <li>update Order: update 01 order</li>
                        <li>check voucher by ID: check thông tin mã giảm giá, quan trọng là có hay không, và % giảm giá </li>
                        <li>get drink list: lấy danh sách đồ uống</li>
                    </ul>
                    <strong className="text-danger"> Bật console log để nhìn rõ output </strong>
                </div>
                </div>
            </div>
            </>
        )
    }
}

export default FecthComponent;