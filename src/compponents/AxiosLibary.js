import axios from "axios";
import { Component } from "react";

class AxiosLibary extends Component {

    axiosLibaryCallApi = async (config) => {
        let response = await axios(config);
        return response.data;
    }

    onBtnGetOrderByIdClick = () => {
        let idOrderCode = "PTyCiXO1I2" 
        let config = {
            method: 'get',
            maxBodyLength: Infinity,
            url: 'http://203.171.20.210:8080/devcamp-pizza365/orders/' + idOrderCode,
            headers: { }
          };

        this.axiosLibaryCallApi(config)
        .then((response) => {
            console.log(response);
        })
        .catch((error) => {
            console.error(error);
        })
    }

    onBtnGetAllOrder = () => {
        let config = {
            method: 'get',
            maxBodyLength: Infinity,
            url: 'http://203.171.20.210:8080/devcamp-pizza365/orders',
          };

          this.axiosLibaryCallApi(config)
          .then((response) => {
            console.log(response);
          })
          .catch((error) => {
            console.log(error);
          })
    }

    onBtnCreateOrderClick = () => {
        var data = JSON.stringify({
            "kichCo": "M",
            "duongKinh": "25",
            "suon": 4,
            "salad": "300",
            "loaiPizza": "HAWAII",
            "idVourcher": "16512",
            "thanhTien": 200000,
            "giamGia": 20000,
            "idLoaiNuocUong": "PEPSI",
            "soLuongNuoc": 3,
            "hoTen": "Lê Quốc Dũng",
            "email": "binhpt001@devcamp.edu.vn",
            "soDienThoai": "0865241654",
            "diaChi": "Hô Chí Minh",
            "loiNhan": "Pizza đế dày",
            "trangThai": "open"
          });

        let config = {
            method: 'post',
            maxBodyLength: Infinity,
            url: 'http://203.171.20.210:8080/devcamp-pizza365/orders',
            headers: { 
                'Content-Type': 'application/json'
              },
            data: data
          };

        this.axiosLibaryCallApi(config)
        .then((response) => {
            console.log(response);
        })
        .catch((error) => {
            console.error(error);
        })
    }

    onBtnUpdateOrderClick = () => {
        let idOrderCode = "180924";
        var data = JSON.stringify({
            "kichCo": "M",
            "duongKinh": "25",
            "suon": 4,
            "salad": "300",
            "loaiPizza": "HAWAII",
            "idVourcher": "16512",
            "thanhTien": 200000,
            "giamGia": 20000,
            "idLoaiNuocUong": "PEPSI",
            "soLuongNuoc": 3,
            "hoTen": "Lê Quốc Dũng update",
            "email": "binhpt001@devcamp.edu.vn",
            "soDienThoai": "0865241654",
            "diaChi": "Hô Chí Minh",
            "loiNhan": "Pizza đế dày",
            "trangThai": "cancel"
          });

        let config = {
            method: 'put',
            maxBodyLength: Infinity,
            url: 'http://203.171.20.210:8080/devcamp-pizza365/orders/' + idOrderCode,
            headers: { 
                'Content-Type': 'application/json'
              },
            data: data
          };

        this.axiosLibaryCallApi(config)
        .then((response) => {
            console.log(response);
        })
        .catch((error) => {
            console.error(error);
        })
    }

    onBtnCheckVoucherIdClick = () => {
        let idVoucher = '123545'
        let config = {
            method: 'get',
            maxBodyLength: Infinity,
            url: 'http://203.171.20.210:8080/devcamp-pizza365/voucher_detail/' + idVoucher,
            headers: { }
          };

        this.axiosLibaryCallApi(config)
        .then((response) => {
            console.log(response);
        })
        .catch((error) => {
            console.error(error);
            console.log('Không tìm thấy idVoucher')
        })
    }

    onBtnGetDrinkListClick = () => {
        let config = {
            method: 'get',
            maxBodyLength: Infinity,
            url: 'http://203.171.20.210:8080/devcamp-pizza365/drinks',
            headers: { }
          };

        this.axiosLibaryCallApi(config)
        .then((response) => {
            console.log(response);
        })
        .catch((error) => {
            console.error(error);
        })
    }

    render() {
        return (
            <div className="bg-light">
                <div className="container">
                    <div className="form-group">
                        <p id="cmt2dev">Test Page for Javascrip Tasks. F5 to run code. </p>
                    </div>
                    <div className="form-group">
                    <form id="singleForm w-75">
                        <input type="button" className="btn btn-primary me-2" onClick= {this.onBtnGetAllOrder} value="Call api get all orders!"/>
                        <input type="button" className="btn btn-info me-2" onClick={this.onBtnCreateOrderClick} value="Call api create order!"/>
                        <input type="button" className="btn btn-success me-2" onClick={this.onBtnGetOrderByIdClick} value="Call api get order by id!"/>
                        <input type="button"  className="btn btn-secondary me-2" onClick={this.onBtnUpdateOrderClick} value="Call api update order!"/>
                        <input type="button"  className="btn btn-danger me-2" onClick={this.onBtnCheckVoucherIdClick} value="Call api check voucher by id!"/>
                        <input type="button"  className="btn btn-success me-2" onClick={this.onBtnGetDrinkListClick}value="Call api Get drink list!"/> 
                    </form>
                </div>
                <div className="form-group">
                    <p id="testP" style={{fontSize:'larger'}}> Demo 06 API for Pizza 365 Project: </p>
                    <ul>
                        <li>get all Orders: lấy tất cả orders </li>
                        <li>create Order: tạo 1 order</li>
                        <li>get Order by ID: lấy 1 order bằng ID </li>
                        <li>update Order: update 01 order</li>
                        <li>check voucher by ID: check thông tin mã giảm giá, quan trọng là có hay không, và % giảm giá </li>
                        <li>get drink list: lấy danh sách đồ uống</li>
                    </ul>
                    <strong className="text-danger"> Bật console log để nhìn rõ output </strong>
                </div>
                </div>
            </div>
        )
    }
}

export default AxiosLibary;