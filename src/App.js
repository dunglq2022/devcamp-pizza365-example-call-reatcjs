import '../node_modules/bootstrap/dist/css/bootstrap.min.css'
import AxiosLibary from './compponents/AxiosLibary';
import FecthComponent from './compponents/FetchLibary';
function App() {
  return (
    <div>
      <AxiosLibary/>
      <FecthComponent/>
    </div>
  );
}

export default App;
